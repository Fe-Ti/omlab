# Copyright 2021 Fe-Ti
# Branches and borders method

from math import floor

from gyouretsu_lib import make_matrix_copy
from canonical_finder_lib import CanonicalFinder, SolverException

# ~ class Bracket:
    # ~ def __init__(self, parameters):
        # ~ self.te = parameters['top_end']
        # ~ self.fc = parameters['fill_char']
        # ~ self.m = parameters['middle']
        # ~ self.be = parameters['bottom_end']
    # ~ def __call__(self, sl):
        # ~ middle_num = len(sl) // 2
        # ~ for i, string in enumerate(sl):
            # ~ if i == 0:
                # ~ sl[i] = self.te + string
            # ~ elif i == middle_num:
                # ~ sl[i] = self.m + string
            # ~ elif i == len(sl)-1:
                # ~ sl[i] = self.be + string
            # ~ else:
                # ~ sl[i] = self.fc + string
# ~ lb = Bracket({'top_end':'⎧','fill_char':'⎪','middle':'⎨','bottom_end':'⎩'})

class ImpossibleOperation(Exception):
    pass

class LPTask:
    '''
    Linear programming task.
    Accepts a dictionary with parameters of a task.
    Anything but lists as a containers is not guaranteed to work.
    Dictionary format:
    {
        "func vector"   :   <any 1 dimensional container behaving like list>,
        "lim matrix"    :   <any 2D containter like list of lists>,
        "lim vector"    :   <any 1D container>,
        "find max"      :   <boolean value>,
        "branching_var" :   <None or (var_number, var_value)>
        "var name"      :   <string> # optional, default is 'x'
    }
    '''
    def __init__(self,
                    lpt_parameters,
                    solve_on_creation = False,
                    print_solution = True,
                    debug = True):
        self.debug = debug
        self.print_solution = print_solution
        self.parameters = lpt_parameters.copy()
        if "var name" not in self.parameters:
            self.parameters["var name"] = 'x'
        self.solution = dict()
        self.bad_var = None
        if solve_on_creation:
            self.solve()

    def log(self, string):
        '''(Internal function)
        Print string if self.debug is True.
        '''
        if self.debug:
            print(string)

    def solve(self, retry_count = 0):
        '''
        Run CanonicalFinder with task parameters.
        Fill up the solution
        '''
        self.log("Initializing solver...")
        self.print_canonical_task_form_string()
        cf = CanonicalFinder(self.parameters["func vector"],
                                self.parameters["lim matrix"],
                                self.parameters["lim vector"],
                                self.parameters["find max"],
                                self.debug)
        cf.make_simplex_table()
        self.log("Trying to solve...")
        try:
            cf.solve(retry_count)
            self.solution['F'] = cf.get_func_extremum()
            self.solution['X'] = cf.get_function_vars()
            self.solution['no solutions'] = False
            self.log("Solved!")
            if self.print_solution:
                self.print_solution_string()
        except SolverException as e:
            self.log(e)
            self.solution['no solutions'] = True

    def is_valid_solution(self):
        if not self.solution:
            raise ImpossibleOperation("Solution is empty")
        if not self.solution['no solutions']:
            for i in self.solution['X']:
                if not float.is_integer(float(self.solution['X'][i])):
                    print(self.solution['X'][i])
                    self.bad_var = i
                    return False
        return True

    def get_bad_var(self):
        '''!
        Get a tuple like (var_num, var_value)
        '''
        return self.bad_var, self.solution['X'][self.bad_var]

    def get_branching_condition(self, var_name = 'x'):
        if not self.parameters["branching_var"]:
            return
        v_num = self.parameters["branching_var"][0]
        if v_num < 0:
            v_num = -v_num
            v_val = -self.parameters["branching_var"][1]
            return f"{var_name}_{v_num} >= {v_val}"
        else:
            v_val = self.parameters["branching_var"][1]
            return f"{var_name}_{v_num} <= {v_val}"
        return

    def print_parameters_to_str(self):
        string = ''
        for i in self.parameters:
            string += str(i) + ' : ' + str(self.parameters[i]) + ', '
        return string[:-2]

    def print_results_to_str(self):
        string = ''
        if self.get_branching_condition():
            string += f"if {self.get_branching_condition()} then "
        for i in self.solution:
            if self.solution[i]:
                if type(self.solution[i]) is bool:
                    string += str(i) + ', '
                else:
                    string += str(i) + ' : ' + str(self.solution[i]) + ', '
        return string[:-2]
    def __str__(self):
        return self.print_results_to_str()
    def __repr__(self):
        return '\n    '+str(self)

    def get_extremum_type(self):
        if self.parameters['find max']:
            return 'max'
        else:
            return 'min'

    def get_canonical_task_form_string(self, do_print_zeros = False):
        var_name = self.parameters['var name']
        extr = self.get_extremum_type()
        string = f"size 14 {{ F({var_name}) ="
        sum_fstring = " {lp} {kx} {var_name}_{i} {rp} +"
        func_postfix = f" }} newline\nsize 14 {{ F -> {extr}, }} newline\nsize 14 {{ left lbrace alignl matrix{{\n"
        m_line_sep = ' ##\n'
        string_end = f"}} right none }}\nnewline\nsize 14 {{{var_name}_i >= 0}}."
        
        func_vector = self.parameters["func vector"]
        lim_vector = self.parameters["lim vector"]
        lim_matrix = self.parameters["lim matrix"]
        for i, kx in enumerate(func_vector):
            if do_print_zeros or kx != 0:
                lp = '('*(kx < 0)
                rp = ')'*(kx < 0)
                if i == 0:
                    string += f" {lp} {kx} {rp} + "
                else:
                    string += sum_fstring.format(i=i, kx=kx, var_name=var_name, lp=lp, rp=rp)
        string = string[:-2] + func_postfix
        slack_counter = len(func_vector)
        for i, row in enumerate(lim_matrix):
            string += '\t'
            for j, kx in enumerate(row, 1):
                if do_print_zeros or kx != 0:
                    lp = '('*(kx < 0)
                    rp = ')'*(kx < 0)
                    if kx == 1 and not do_print_zeros:
                        kx = ''
                    string += sum_fstring.format(i=j, kx=kx, var_name=var_name, lp=lp, rp=rp)
            if not do_print_zeros:
                string += f" {var_name}_{slack_counter}"
            else:
                string += f" 1 {var_name}_{slack_counter}"
            string = string + f" = {lim_vector[i]}" + m_line_sep*(i!=len(lim_matrix) - 1)
            slack_counter += 1
        string += string_end
        string = string.replace('/', " over ")
        return string

    def print_canonical_task_form_string(self, do_print_zeros = False):
        print(self.get_canonical_task_form_string(do_print_zeros))

    def get_solution_string(self):
        var_name = self.parameters['var name']
        string = f"size 14 {{ F = {self.solution['F']} = {float(self.solution['F']):f} }} newline\nsize 14 {{ "
        for i, x in enumerate(self.solution['X'], 1):
            string += f"{var_name}_{i} = {self.solution['X'][x]}, "
        string = string [:-2] + ' }'
        string = string.replace('/', " over ")
        return string

    def print_solution_string(self):
        print(self.get_solution_string())

class BBSolver:
    '''
    Branches and borders method solver class.
    Accepts a dictionary with parameters of an initial task.
    Anything but lists as a containers is not guaranteed to work.
    Dictionary format:
    {
        "func vector"   :  <any 1 dimensional container behaving like list>,
        "lim matrix"    :  <any 2D containter like list of lists>,
        "lim vector"    :  <any 1D container>,
        "find max"      :  <boolean value>
    }
    '''
    def __init__(self,
                    parameters,
                    debug = True):
        self.debug = debug
        if "branching_var" not in parameters:
            parameters["branching_var"] = None
        self.solution_tree = {"root" : LPTask(parameters, debug=self.debug)}

    def log(self, string):
        '''(Internal function)
        Print string if self.debug is True.
        '''
        if self.debug:
            print(string)

    def make_branching(self, node):
        new_params_low = node.parameters.copy()
        new_params_low["lim vector"] = new_params_low["lim vector"][:]
        new_params_low["lim matrix"] = make_matrix_copy(new_params_low["lim matrix"])
        new_params_high = node.parameters.copy()
        new_params_high["lim vector"] = new_params_high["lim vector"][:]
        new_params_high["lim matrix"] = make_matrix_copy(new_params_high["lim matrix"])

        bad_var_num = int(node.get_bad_var()[0]) - 1
        bad_var_val = floor(node.get_bad_var()[1])
        row = [ 0 for i in enumerate(new_params_low["lim matrix"][0]) ]
        row[bad_var_num] = 1

        new_params_low["branching_var"] = (bad_var_num+1, bad_var_val)
        new_params_high["branching_var"] = (-bad_var_num-1, -bad_var_val - 1)

        new_params_low["lim vector"].append(bad_var_val)
        new_params_low["lim matrix"].append(row)
        new_params_high["lim vector"].append(-bad_var_val - 1)
        new_params_high["lim matrix"].append([-i for i in row])
        
        self.solution_tree[node] = [
                                    LPTask(new_params_low, debug=self.debug),
                                    LPTask(new_params_high, debug=self.debug),
                                    ]

    def solve_tree(self, node, retry_count = 0):
        node.solve(retry_count)
        if not node.is_valid_solution():
            self.make_branching(node)
            self.solve_tree(self.solution_tree[node][0], retry_count)
            self.solve_tree(self.solution_tree[node][1], retry_count)

    def solve(self, retry_count = 0):
        self.solve_tree(self.solution_tree['root'], retry_count)

    def get_best_solution_rec(self, node, bs, cmp_fnc):
        if node in self.solution_tree:
            self.get_best_solution_rec(self.solution_tree[node][0], bs, cmp_fnc)
            self.get_best_solution_rec(self.solution_tree[node][1], bs, cmp_fnc)
        elif not node.solution['no solutions']:
            if not bs:
                bs['F'] = node.solution['F']
                bs[0] = node.solution['X']
            elif cmp_fnc(node.solution['F'], bs['F']):
                bs.clear()
                bs['F'] = node.solution['F']
                bs[0] = node.solution['X']
            elif node.solution['F'] == bs['F']:
                bs[len(bs) - 1] = node.solution['X']

    def get_best_solution(self):
        bs = dict()
        node = self.solution_tree['root']
        if self.solution_tree['root'].parameters["find max"]:
            cmp_fnc = (lambda a,b: a > b)
        else:
            cmp_fnc = (lambda a,b: a < b)
        if node in self.solution_tree:
            self.get_best_solution_rec(self.solution_tree[node][0], bs, cmp_fnc)
            self.get_best_solution_rec(self.solution_tree[node][1], bs, cmp_fnc)
        return bs

    def __str__(self):
        bs = self.get_best_solution()
        string = f"F = {bs['F']}\nPossible solutions:\n"
        for i in range(len(bs) - 1):
            string += f"X_{i} = {bs[i]}\n"
        return string

    def print_tree_0(self, node, prefix = ''):
        if node in self.solution_tree:
            print(prefix + '┣', node, sep='━')
            self.print_tree_0(self.solution_tree[node][0], prefix + ' ┃')
            self.print_tree_1(self.solution_tree[node][1], prefix + ' ┃')
        else:
            print(prefix + '┣', node, sep='━')

    def print_tree_1(self, node, prefix = ''):
        if node in self.solution_tree:
            print(prefix + '┗', node, sep='━')
            self.print_tree_0(self.solution_tree[node][0], prefix + '  ')
            self.print_tree_1(self.solution_tree[node][1], prefix + '  ')
        else:
            print(prefix + '┗', node, sep='━')

    def print_solution_tree(self):
        node = self.solution_tree['root']
        print('→',node,sep='')
        if node in self.solution_tree:
            self.print_tree_0(self.solution_tree[node][0], ' ')
            self.print_tree_1(self.solution_tree[node][1], ' ')

# Copyright 2021 Fe-Ti
