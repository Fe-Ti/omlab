# Lab 01
# Copyright 2021 Fe-Ti
# Simplex Method (Algorithm)

from bunsuu_lib import Fraction
import canonical_finder_lib as cfl

print('\n\nExample with stock types (int + float)\n\n')
cfn = cfl.CanonicalFinder([0,3,1, 4],
                        [
                            [ 2,  1, 1],
                            [ 1, 4, 0],
                            [ 0,  0.5, 1],
                        ],
                        [
                            6,
                            4,
                            1
                        ],
                        find_max=True
                    )
cfn.make_simplex_table()
cfn.solve_and_print()

print('\n\nExample with Fraction class from bunsuu_lib\n\n')
cff = cfl.CanonicalFinder([Fraction(0),Fraction(3),Fraction(1), Fraction(4)],
                        [
                            [ Fraction(2),  Fraction(1), Fraction(1)],
                            [ Fraction(1), Fraction(4), Fraction(0)],
                            [ Fraction(0),  Fraction('1/2'), Fraction(1)],
                        ],
                        [
                            Fraction(6),
                            Fraction(4),
                            Fraction(1)
                        ],
                        find_max=True
                    )
cff.make_simplex_table()
cff.solve_and_print()
# Copyright 2021 Fe-Ti
