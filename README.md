# Optimization Methods Labs
List of lab works:

* l-01 (Simplex method);
* l-02 (Ambivalent problem);
* l-03 (Branches and borders method);
* l-04 (MC optimization);
* l-05 (Filtration);
* l-06 (Game theory).

List of libs:

* `ambivalent_problem_lib.py`;
* `bb_method.py` (branches and borders)
* `bunsuu_lib.py` (Fraction class);
* `canonical_finder_lib.py`;
* `gyouretsu_lib.py` (matrix operation lib);
* `simplex_algorithm_lib.py`;
* `udezuku_lib.py` (bruteforcer for bb lab);
* `game_theory_lib.py` (some sort of a library);
* `hyou_lib.py` (Table class and etc);
* `mco_lib.py`.

Other stuff:

* `highlighter.sh` is a script for highlighting the code;
* `themes` is a directory where themes for highlighting are located;
* `doxygen`: Doxygen related files for generating [documentation](https://fe-ti.gitlab.io/omlab/index.html)... E-e-e, well, sort of.

### Disclaimer
The libraries are unstable. There are a lot of bugs.
So if nothing works or|and your computer crashes, starts flying or whatever, don't panic.
Just feel fine freeing of computer addiction.

### License: MPL 2.0

```
Made with love
Copyright 2021-2022 Fe-Ti
```
