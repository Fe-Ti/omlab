# Lab 04
# Copyright 2021 Fe-Ti
# Multicriteria optimization
from mco_lib import *
from gyouretsu_lib import multiplicate, pretty_str, transpose_matrix

weights_vec = [5, 7, 3, 3]
print(pretty_str([weights_vec], 7))
norm_weights_vec = normalize_weights(weights_vec)
print(pretty_str([norm_weights_vec], 7))

t = Table(
            {
                "title" :   ["V\\C", "Стипендия", "Квалификация", "Стоимость", "Престижность"],
                "rows"  :   [
                                ["МГУ",    7,  7,  7,  8],
                                ["МФТИ",   3,  5,  5,  7],
                                ["СГУ",    2,  3,  2,  2],
                                ["Oxford", 0,  8,  9,  9]
                            ]
            }
        )

c = {
        "main"  :   2,
        "values":   {
                        1               :   0.4,
                        3               :   -0.5,
                        4               :   0.3,
                    },
        "criteria type" : "NLTM"
    }

print(t)
mcp = MainCriteriaProcessor(c, t)
print(mcp)

# Why?
main_criteria = ['Дешевизна', "Долговечность"]
main_criteria = ['Квалификация', "Стоимость"]
print("\n## Pareto Set ##")
print(f"Using {main_criteria} as main criteria.")
print("\nMatlab commands to plot Pareto set:")

c1 = t.get_column(main_criteria[0])
c2 = t.get_column(main_criteria[1])

dots = [ (v, c2[i]) for i, v in enumerate(c1) ]

s = str([ max(c1), min(c2)] + dots).replace('(','').replace(')','').replace('[','').replace(']','')
print(f"H = plot({s});")
print('legd = legend("Utopia", "A", "B", "C", "D");')
print('set (legd, "fontsize", 16)')
print('set (legd, "location", "northeastoutside")')
print('set(H, "linewidth", 15);')
print('set(H, "MarkerSize", 20);')
print(f'xlabel("{main_criteria[0]}")')
print(f'ylabel("{main_criteria[1]}")')

def manhattan_dist(utopia, dot):
    return sum([abs(f - utopia[i]) for i, f in enumerate(dot)])

print("\nUsing Manhattan style of distance.")

utopia = ( max(c1), min(c2) )
min_d = manhattan_dist(utopia, (0,0))
variant = -1
for i, dot in enumerate(dots, 65):
    print(manhattan_dist(utopia, dot), dot)
    if min_d > manhattan_dist(utopia, dot):
        min_d = manhattan_dist(utopia, dot)
        variant = i
print(f"The minimal distance is {min_d} between variant {chr(variant)}{dots[variant-65]} and Utopia{utopia}.")
print(f"Variant {chr(variant)} is {t.get_column(0)[variant-65]}.")


print("\n\n#    Fudging criteria together method    #\n\n")
def normalize_table(t, by_max=True):
    t = t.copy()
    column_maxs = [ max(t.get_column(i)) for i in t.title[1:] ]
    column_mins = [ min(t.get_column(i)) for i in t.title[1:] ]
    # ~ print (column_maxs)
    # ~ print (column_mins)
    for i, row in enumerate(t):
        for j, element in enumerate(row[1:], 1):
            if by_max:
                k = j - 1
                t[i].columns[j] = (element - column_mins[k]) / (column_maxs[k] - column_mins[k])
    t.update_column_widths()
    t.name = "Normalized " + t.name
    return t

# Let's say that we have something like that
# c1            c2              c3              c4
# "Стипендия",  "Квалификация", "Стоимость",    "Престижность"
#  _
# | c1 > c2  y12 == 1
# | c1 > c3  y13 == 1
# | c1 > c4  y14 == 1
#<  c2 > c3  y23 == 1
# | c2 > c4  y24 == 1
# | c3 ~ c4  y34 == 0.5
# |_
#
# wights are
weights = [
            1+1+1,
            0+1+1,
            0+0+0.5,
            0+0+0.5,
        ]
norm_weights = normalize_weights(weights)
norm_t = normalize_table(t)

# Looks awful but whatever
print("Current weights are:")
print(pretty_str([t.title.columns[1:], weights], max(t.column_widths)))
print("After normalization:")
print(pretty_str([t.title.columns[1:], norm_weights], max(t.column_widths)))
print("### Multiplying ###", norm_t, "by", pretty_str([norm_weights], 10), sep="\n")
res = multiplicate(norm_t.to_list(), norm_weights)
print("The result is:", pretty_str([["Result"]]+res, 20), sep="\n")
print("The best is: ", t.get_column(0)[res.index(max(res))])



print("\n\n##############\n#    AHP     #")

criteria_cmp = Table(
            {
                "name" : "Попарное сравнение критериев",
                "title" :   ["C\\C", "Стипендия", "Квалификация", "Стоимость", "Престижность"],
                "rows"  :   [
                                ["Стипендия"    ,   1,      2,      3,      3],
                                ["Квалификация" ,   0.5,    1,      2,      5],
                                ["Стоимость"    ,   0.333,  0.5,    1,      4],
                                [ "Престижность",   0.333,  0.2,    0.25,  1]
                            ]
            }
        )

grants_cmp = Table(
            {
                "name" : "Попарное сравнение стипендий",
                "title" :   ["V\\V", "МГУ", "МФТИ", "СГУ", "Oxford"],
                "rows"  :   [
                                ["МГУ",    1,       2,      5,      8],
                                ["МФТИ",   0.5,     1,      3,      6],
                                ["СГУ",    0.2,     0.333,  1,      5],
                                ["Oxford", 0.143,   0.166,  0.2,    1]
                            ]
            }
        )

quality_cmp = Table(
            {
                "name" : "Попарное сравнение квалификации",
                "title" :   ["V\\V", "МГУ", "МФТИ", "СГУ", "Oxford"],
                "rows"  :   [
                                ["МГУ",    1,       3,      5,      0.333],
                                ["МФТИ",   0.333,   1,      3,      0.2],
                                ["СГУ",    0.2,     0.333,  1,      0.143],
                                ["Oxford", 3,       5,      7,      1]
                            ]
            }
        )

price_cmp = Table(
            {
                "name" : "Попарное сравнение стоимости",
                "title" :   ["V\\V", "МГУ", "МФТИ", "СГУ", "Oxford"],
                "rows"  :   [
                                ["МГУ",    1,       0.333,  0.166,  2],
                                ["МФТИ",   3,       1,      0.25,    5],
                                ["СГУ",    6,       4,      1,      7],
                                ["Oxford", 0.5,     0.2,    0.143,  1]
                            ]
            }
        )

prestige_cmp = Table(
            {
                "name" : "Попарное сравнение престижности",
                "title" :   ["V\\V", "МГУ", "МФТИ", "СГУ", "Oxford"],
                "rows"  :   [
                                ["МГУ",    1,       3,      5,  0.333   ],
                                ["МФТИ",   0.333,   1,      3,  0.2     ],
                                ["СГУ",    0.2,     0.333,  1,  0.143   ],
                                ["Oxford", 3,       5,      7,  1       ]
                            ]
            }
        )

def get_priority_vec(mx, return_raw=False):
    priority_vec_raw = [ sum(i) for i in mx ]
    if return_raw:
        return priority_vec_raw
    pv_sum = sum(priority_vec_raw)
    return [ i/pv_sum for i in priority_vec_raw ]

def get_lambda_max(t, pv):
    lm = 0
    for i, row_element in enumerate(pv,1):
        f_cking_column_sum = sum(t.get_column(i))
        lm += row_element*f_cking_column_sum
    return lm

def get_consistency_ratio(table):
    mx = table.get_matrix()
    lm = get_lambda_max(table, get_priority_vec(mx))
    return (lm - len(mx))/(len(mx)-1)

print(criteria_cmp)
print("Отношение согласованности", get_consistency_ratio(criteria_cmp))

print(grants_cmp)
print("Отношение согласованности", get_consistency_ratio(grants_cmp))

print(quality_cmp)
print("Отношение согласованности", get_consistency_ratio(quality_cmp))

print(price_cmp)
print("Отношение согласованности", get_consistency_ratio(price_cmp))

print(prestige_cmp)
print("Отношение согласованности", get_consistency_ratio(prestige_cmp))

crit_mx = transpose_matrix([ get_priority_vec(i.get_matrix()) for i in [ grants_cmp, quality_cmp, price_cmp, prestige_cmp ] ])
crit_vec = get_priority_vec(criteria_cmp.get_matrix())

print("Multiplying",pretty_str(crit_mx, 7), sep='\n')
print("by",pretty_str([crit_vec], 7), sep='\n')
res = multiplicate(crit_mx, crit_vec)
print("The result is",pretty_str(res, 7), sep='\n')
print("The best is", t.get_column(0)[res.index(max(res))])

# Copyright 2021 Fe-Ti 
