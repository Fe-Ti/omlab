# Copyright 2021 Fe-Ti
# Bruteforcer for branches and borders method lab work
# Japanese 'udezuku' means bruteforce

from math import floor, isnan

from gyouretsu_lib import make_matrix_copy
from canonical_finder_lib import CanonicalFinder, SolverException


class BForcer:
    def log(self, string):
        '''(Internal function)
        Print string if self.debug is True.
        '''
        if self.debug:
            print(string)

    def __init__(self,
                    parameters,
                    solve_on_creation = True,
                    debug = True):
        self.debug = debug
        ## 'function vector'
        self.fv = parameters["func vector"][:]
        ## 'limiting matrix'
        self.lm = make_matrix_copy(parameters["lim matrix"])
        ## 'limiting vector'
        self.lv = parameters["lim vector"][:]
        ## 'find max'
        self.fmax = parameters["find max"]
        self.solution = dict()
        if solve_on_creation:
            self.solve()

    def get_limits(self):
        '''
        Get a vector of upper limits for vars
        '''
        limits = [ float('inf') for i in self.lm[0] ]
        for var_num, elem in enumerate(limits):
            for row_num, row in enumerate(self.lm):
                if row[var_num] != 0:
                    if floor(self.lv[row_num]/row[var_num]) < elem:
                        limits[var_num] = floor(self.lv[row_num]/row[var_num])
        return limits

    def update_vv(self, var_vals, limits):
        var_vals[0] += 1
        #print(var_vals[1:])
        for i, elem in enumerate(var_vals[1:]):
            if var_vals[i] > limits[i]:
                var_vals[i+1] += 1
                var_vals[i] = 0

    def get_fnc_val(self, var_vals):
        fnc = self.fv[0]
        for i, elem in enumerate(var_vals):
            fnc += elem * self.fv[i + 1]
        return fnc

    def is_valid(self, var_vals):
        rs = 0
        for i, row in enumerate(self.lm):
            for j, elem in enumerate(row):
                rs += elem * var_vals[j]
            if rs > self.lv[i]:
                return False
            rs = 0
        return True

    def find_extremum(self, limits, cmp_fnc):
        var_vals = [ 0 for i in limits ]
        ext_val = float('nan')
        ext_solutions = []
        while limits != var_vals:
            if self.is_valid(var_vals):
                self.log(f"F = {self.get_fnc_val(var_vals)}, X = {var_vals}")
                if (cmp_fnc(ext_val, self.get_fnc_val(var_vals)) or isnan(ext_val)):
                    ext_val = self.get_fnc_val(var_vals)
                    ext_solutions = [var_vals[:]]
                    self.log(f"New extremum F = {ext_val}")
                elif ext_val == self.get_fnc_val(var_vals) and not isnan(ext_val):
                    ext_solutions.append(var_vals[:])
                    self.log(f"Added another solution with F = {ext_val}")
            self.update_vv(var_vals, limits)
        return ext_val, ext_solutions

    def find_max(self, limits):
        return self.find_extremum(limits, (lambda a, b: a<b))

    def find_min(self, limits):
        return self.find_extremum(limits, (lambda a, b: a>b))

    def solve(self):
        self.log("Start bruteforcing...")
        limits = self.get_limits()
        if self.fmax:
            s = self.find_max(limits)
        else:
            s = self.find_min(limits)
        self.solution['F'] = s[0]
        for n, x in enumerate(s[1],1):
            self.solution[n] = {str(i) : e for i,e in enumerate(x, 1)}

    def __str__(self):
        string = f"F = {self.solution['F']}\nPossible solutions:\n"
        for i in range(len(self.solution) - 1):
            string += f"X_{i} = {self.solution[i+1]}\n"
        return string
