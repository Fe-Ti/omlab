# Copyright 2021 Fe-Ti
# MCO library

from hyou_lib import Table

def to_float(num):
    if type(num) is str:
        if num.strip().endswith('%'):
            return float(num.strip()[:-1]) * 0.01
    return float(num)

def normalize_weights(weights):
    wsum = sum(weights)
    return [ i/wsum for i in weights ]

def not_less_than_share_of_cmax(value, share, column):
    return value >= max(column) * to_float(share)

def not_more_than_share_of_cmax(value, share, column): # share must be < -1
    return value <= max(column) * abs(to_float(share))

def check_item(value, share, column): # actually there should be more functions
                                      # but who cares
    if to_float(share) < 0:
        return not_more_than_share_of_cmax(value, share, column)
    else:
        return not_less_than_share_of_cmax(value, share, column)

class MainCriteriaProcessor:
    def __init__(self, criteria, table):
        '''
        criteria = {
            "main" : <title string or number>, # count numbers from 1
            "values" : {
                    criteria1 name (str) or number (int) : value1 (int),
                    criteria2 : value2,
                    ...
                    criteriaN : valueN,
                    # if valueN <= -1 then using not_more_than_share_of_cmin
                    # with parameters (value, abs(share), column)
                    # where share MUST(!) be less or equal to -1
                }
        }
        table = Table(...)
        '''
        self.table = table.copy()
        self.criteria = criteria.copy()
        self.criteria["values"] = dict()
        for i in criteria["values"]:
            if type(i) is str:
                self.criteria["values"][table.title.columns.index(i)] = criteria["values"][i]
            else:
                self.criteria["values"][i] = criteria["values"][i]
        if type(self.criteria["main"]) is str:
            self.criteria["main"] = table.title.columns.index(self.criteria["main"])
        # ~ print(criteria, "\n\n\n", self.criteria)
        self.normalize_table()
        self.acceptable = list()
        self.find_acceptable()

    def normalize_table(self):
        column_maxs = [ max(self.table.get_column(i)) for i in self.table.title[1:] ]
        column_mins = [ min(self.table.get_column(i)) for i in self.table.title[1:] ]
        for i, row in enumerate(self.table):
            for j, element in enumerate(row[1:], 1):
                if not (j == self.criteria["main"]):
                    k = j - 1
                    self.table[i].columns[j] = (element - column_mins[k]) / (column_maxs[k] - column_mins[k])
        self.table.update_column_widths()
        self.table.name = "Normalized " + self.table.name

    def is_acceptable(self, var):
        for i, value in enumerate(var, 1):
            if i == self.criteria["main"]:
                continue
            if not check_item(
                        value,
                        self.criteria["values"][i],
                        self.table.get_column(i)
                       ):
                return False
        return True

    def find_acceptable(self):
        self.acceptable = list()
        for variant in self.table:
            if self.is_acceptable(variant[1:]):
                 self.acceptable.append(variant)

    def get_acceptable(self, return_string = False):
        if return_string:
            string = "[\n"
            for i in self.acceptable:
                string += f"\t{i}\n"
            return string + "]"
        return self.acceptable

    def get_best(self, find_max=True):
        acceptable = self.acceptable
        column = self.criteria["main"]
        max_elem = acceptable[0][column]
        max_i = 0
        for i, elem in enumerate(acceptable):
            if (find_max and elem[column] > max_elem) or (not find_max and elem[column] < max_elem):
                max_elem = elem[column]
                max_i = i
        return (acceptable[max_i][0], max_elem)

    def __str__(self):
        string = "## Main Criteria Method ##\n"
        string += f"The main criteria is {self.table.title[self.criteria['main']]}\n"
        string += f"Criteria limitations:\n"
        for i in self.criteria["values"]:
            if i!="main":
                if to_float(self.criteria['values'][i]) >= 0:
                    string += f"{self.table.title[i]} must not be less than {self.criteria['values'][i]} of maximum\n"
                else:
                    string += f"{self.table.title[i]} must not be more than {abs(to_float(self.criteria['values'][i]))} of maximum\n"
        string += self.table.__str__()
        string += f"\nList of acceptable variants:\n{self.get_acceptable(True)}\n"
        string += f"The best is: {self.get_best()}"
        return string
