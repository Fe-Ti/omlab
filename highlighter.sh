
echo "Enter directory path:"
read directory
source_type='\.py'

font_size=12
theme_name="moe2"
cnt=0

export HIGHLIGHT_OPTIONS="--stdout  --font-size=${font_size} --inline-css -O html --style=${theme_name}"

echo '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>Source Code</title></head><body>' > source_code.html

for i in `ls $directory | grep $source_type`
do
    cnt=$(($cnt + 1))
    echo "<span style=\"font-size:${font_size}pt\">Программа ${cnt} ‒ ${i}</span>" >> source_code.html
    highlight ${directory}/${i} | grep -v --regexp="title\|body\|html\|head\|meta" >> source_code.html
    echo "File ${cnt} ‒ $i"
done
echo "</body></html>" >> source_code.html

echo "Checkout source_code.html"

