# Lab 06
# Copyright 2021 Fe-Ti
# Game theory & Simplex method
from game_theory_lib import AntagonisticGame_P2
from bunsuu_lib import Fraction

# ~ test_table =    [
                    # ~ [1, 3,  9,  6],
                    # ~ [2, 6,  2,  3],
                    # ~ [7, 2,  6,  5]
                # ~ ]

# ~ test_table =    [
                    # ~ [Fraction(1), Fraction(3),  Fraction(9),  Fraction(6)],
                    # ~ [Fraction(2), Fraction(6),  Fraction(2),  Fraction(3)],
                    # ~ [Fraction(7), Fraction(2),  Fraction(6),  Fraction(5)]
                # ~ ]

test_table =    [
                    [Fraction(4), Fraction(4),  Fraction(0),  Fraction(6),  Fraction(12)],
                    [Fraction(1), Fraction(14),  Fraction(14),  Fraction(13),  Fraction(11)],
                    [Fraction(17), Fraction(6),  Fraction(14),  Fraction(4),  Fraction(3)],
                    [Fraction(18), Fraction(16),  Fraction(13),  Fraction(15),  Fraction(16)]
                ]

# ~ test_table =    [
                    # ~ [4, 4,  0,  6,  12],
                    # ~ [1, 14,  14,  13,  11],
                    # ~ [17, 6,  14,  4,  3],
                    # ~ [18, 16,  13,  15,  16]
                # ~ ]

test = AntagonisticGame_P2(test_table)
print(test)
