from bunsuu_lib import Fraction
from gyouretsu_lib import copy_matrix, make_matrix_copy, dump_matrix_to_str
# Copyright 2021 Fe-Ti
# Simplex algorithm library

class SolverException(Exception):
    pass

class SimplexSolver:
    '''
    Matrix general overview:

    ___|_s_|_x_|_x_|...|_x_|  <-- Free stuff
    _y_|___|___|___|...|___|          +
     y |___|___|___|...|___|      Free vars
    ... ... ... ... ... ...
     y |___|___|___|...|___|
     F |___|___|___|...|___|
     ^
     |
     |
    Basic stuff + Function

    '''
    func_extremum = 'Simplex table is not solved'

    def __init__(self,
                    matrix = list(),
                    debug = True):
        self.matrix = make_matrix_copy(matrix)

        self.legend_horizontal = [str(i) for i,elem in enumerate(matrix[0])]
        self.legend_horizontal[0] = 's'
        self.legend_vertical = [str(i) for i,elem in enumerate(matrix,
                                                len(self.legend_horizontal))]
        self.legend_vertical[-1] = 'F'
        self.debug = debug

    def log(self, string):
        if self.debug:
            print(string)

    def is_valid_solution(self):
        '''
        Checking if all values
        in the 1st column >= 0 (except func. row)
        '''
        for i in self.matrix[:-1]:
            if i[0] < 0:
                return False
        return True

    def is_optimized_solution(self):
        '''
        Checking if all values
        in the last (func.) row are <= 0
        '''
        for i in self.matrix[-1][1:]:
            if i > 0:
                #print(False)
                return False
        #print(True)
        return True

    def find_bad_row(self):
        for i, row in enumerate(self.matrix[:-1]):
            if row[0] < 0:
                return i

    def find_solving_pivot_column(self, bad_row):
        for i, number in enumerate(self.matrix[bad_row][1:], 1): # going from 1
            if number < 0:
                return i
        raise SolverException("There is no valid solution.")

    def find_optimizing_pivot_column(self):
        for i, number in enumerate(self.matrix[-1][1:], 1): # going from 1
            if number > 0:
                return i
        return -1

    def find_solving_pivot_row(self, pivot_column, previous_pivot_pair):
        min_positive_ratio_row = -1
        min_pos_ratio = -1
        for i, row in enumerate(self.matrix[:-1]):
            try:
                curr_rat = row[0] / row[pivot_column]   # Current ratio
            except Exception:
                curr_rat = -1
            #print(f"Current Ratio: {str(curr_rat):5}, Row_{i}[0]: {str(row[0]):5}, Row_{i}[PC={str(pivot_column)}]: {str(row[pivot_column]):5}, is acceptable: {curr_rat > 0}")
            if curr_rat > 0 and (curr_rat < min_pos_ratio or min_pos_ratio < 0):
                if not (i == previous_pivot_pair[0] and pivot_column == previous_pivot_pair[1]):
                    min_positive_ratio_row = i
                    min_pos_ratio = curr_rat
        if min_pos_ratio == -1 or min_positive_ratio_row == -1:
            raise SolverException("There is no optimal solution.")
        return min_positive_ratio_row

    def find_optimizing_pivot_row(self, pivot_column, previous_pivot_pair):
        min_positive_ratio_row = -1
        min_pos_ratio = -1
        for i, row in enumerate(self.matrix[:-1]):
            if row[pivot_column] > 0:
                try:
                    curr_rat = row[0] / row[pivot_column]   # Current ratio
                except Exception:
                    curr_rat = -1
                if curr_rat >= 0 and (curr_rat < min_pos_ratio or min_pos_ratio < 0):
                    if not (i == previous_pivot_pair[0] and pivot_column == previous_pivot_pair[1]):
                        min_positive_ratio_row = i
                        min_pos_ratio = curr_rat
        if min_pos_ratio == -1 or min_positive_ratio_row == -1:
            raise SolverException("There is no optimal solution.")
        return min_positive_ratio_row

    def swap_vars(self, pivot_row, pivot_column):
        self.log("Swapping x_" + self.legend_vertical[pivot_row] +
                    " and x_" + self.legend_horizontal[pivot_column] + "...")
        buff = self.legend_vertical[pivot_row]
        self.legend_vertical[pivot_row] = self.legend_horizontal[pivot_column]
        self.legend_horizontal[pivot_column] = buff

        mx_copy = make_matrix_copy(self.matrix)

        pivot_element = self.matrix[pivot_row][pivot_column]

        for y, row in enumerate(self.matrix):
            for x, elem in enumerate(row):
                if x == pivot_column and y == pivot_row:
                    self.matrix[y][x] = 1 / pivot_element
                elif x == pivot_column:
                    self.matrix[y][x] = - elem / pivot_element
                elif y == pivot_row:
                    self.matrix[y][x] = elem / pivot_element
                else:
                    self.matrix[y][x] = elem - (mx_copy[y][pivot_column] * mx_copy[pivot_row][x]) / pivot_element

    def get_valid_solution(self):
        self.log("Started getting valid solution...")
        if self.debug:
            self.print_mx()

        previous_pivot_pair = (None, None)
        while not self.is_valid_solution():
            pivot_column = self.find_solving_pivot_column(self.find_bad_row())
            pivot_row = self.find_solving_pivot_row(pivot_column, previous_pivot_pair)
            self.swap_vars(pivot_row, pivot_column)
            if self.debug:
                self.print_mx()
            previous_pivot_pair = (pivot_row, pivot_column)

        self.log("Got valid solution.")

    def optimize_solution(self, retry_count = 0):
        self.log("Started getting optimized solution...")
        if self.debug:
            self.print_mx()
        previous_pivot_pair = (None, None)
        while not self.is_optimized_solution():
            pivot_column = self.find_optimizing_pivot_column() # looking at the last row
            pivot_row = self.find_optimizing_pivot_row(pivot_column, previous_pivot_pair)
            self.swap_vars(pivot_row, pivot_column)
            if self.debug:
                self.print_mx()
            if not self.is_valid_solution():
                if retry_count == 0:
                    raise SolverException("Something went wrong, invalid solution when optimizing.")
                else:
                    retry_count -= 1
                    self.get_valid_solution()
            previous_pivot_pair = (pivot_row, pivot_column)

        self.log("Finished optimization.")

    def solve(self, retry_count = 0):
        '''
        Solve the tableau or raise an exception (SolverException).
        '''
        try:
            self.get_valid_solution()
            self.optimize_solution(retry_count)
            self.func_extremum = self.matrix[-1][0]
        except SolverException as error_msg:
            raise SolverException(f"Error while solving: {error_msg}")

    def dump_mx(self, invert_val_in_func=False):
        '''
        Dumping matrix (simplex tableau) to a string (used in CanonicalFinder
        class for printing).
        '''
        mx = make_matrix_copy(self.matrix)
        if invert_val_in_func:
            mx[-1] = [-i for i in mx[-1]]
        for i,row in enumerate(mx):
            if self.legend_vertical[i] != 'F':
                mx[i] = ['X_'+self.legend_vertical[i]] + mx[i]
            else:
                mx[i] = [self.legend_vertical[i]] + mx[i]
        mx.insert(0, ['*']+['X_'+i for i in self.legend_horizontal])
        return dump_matrix_to_str(mx)

    def print_mx(self):
        print(self.dump_mx())

    def get_basic_vars(self):
        '''
        Get all of your basic variables.
        Returns a dict like {'varname': varvalue}
        '''
        basic_vars = dict()
        for i, row in enumerate(self.matrix[:-1]):
            basic_vars[self.legend_vertical[i]] = row[0]
        return basic_vars

    def get_function_vars(self):
        '''
        Filter basic variables by being slack, e.i. the ones
        which are presented in function.
        Also returns variables which are equal to zero.
        '''
        var_count = len(self.matrix[-1])
        func_vars = dict()
        basic_vars = self.get_basic_vars()
        for i in range(1, var_count):
            if str(i) in basic_vars:
                func_vars[str(i)] = basic_vars[str(i)]
            else:
                func_vars[str(i)] = 0
        return func_vars
# Copyright 2021 Fe-Ti
