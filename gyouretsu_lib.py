# Copyright 2021 Fe-Ti
# Matrix Operation Lib
'''
"mx" is usually a matrix copy
'''

def copy_matrix(source, dest):
    if type(source) is not list or type(dest) is not list:
        raise TypeError('The arguments must be lists')

    dest.clear()
    for row in source:
        dest.append(row[:])

def make_matrix_copy(source):
    mx = list()
    copy_matrix(source, mx)
    return mx

def dump_matrix_to_str(source_mx, length = -1, precision = 4, sep = "|", pretty_print = True):
    '''
    Dump matrix to string. By default element length is guessed (length=-1).
    '''
    string = ''
    cw = [ length for i in source_mx[0] ] # column widths
    if length < 0:
        for row in source_mx:
            for i, elem in enumerate(row):
                if type(elem) is float:
                    if cw[i] < len(f"{elem:.{precision}f}"):
                        cw[i] = len(f"{elem:.{precision}f}")
                else:
                    if cw[i] < len(str(elem)):
                        cw[i] = len(str(elem))
    if not pretty_print:
        for row in source_mx:
            for i, elem in enumerate(row):
                if type(elem) is float:
                    string+=f"{elem:>{cw[i]}.{precision}f}{sep}"
                else:
                    string+=f"{str(elem):>{cw[i]}}{sep}"
            string+='\n'
    else:
        string = pretty_str(source_mx, cw, passed_cwv=True)
    return string

def transpose_matrix(matrix):
    mx = list()

    for i, row in enumerate(matrix):
        for j, elem in enumerate(row):
            if i == 0:
                mx.append([elem])
            else:
                mx[j].append(elem)

    return mx

def mul_by_num(n, mx):
    result = list()
    for row in mx:
        result.append([ n * i for i in row ])
    return result

def multiplicate(a, b):
    """
    Multiplicate two matrixes.
    """
    if type(a) is not list:
        result = mul_by_num(a, b)
    elif type(b) is not list:
        result = mul_by_num(b, a)
    else:
        if len(a[0]) != len(b):
            raise IndexError("Column count of A must be equal to row count of B")
        def msum(a, i, b, j = False):
            """(internal function)
            Returns s += a[i][n] * b[n][j]
            """
            s = 0
            for n, row in enumerate(b):
                if not j:
                    s += a[i][n] * row
                else:
                    s += a[i][n] * row[j]
            return s
        result = list()
        resrow = 0
        for i, row in enumerate(a):
            result.append(list())
            resrow = result[-1]
            if type(b[0]) is list:
                for j, element in enumerate(b[0]):
                    resrow.append(msum(a, i, b, j))
            else:
                resrow.append(msum(a, i, b))
    return result

# pretty print function

HL = '━'

LML = '┣'
LUC = '┏'
LBC = '┗'

RML = '┫'
RUC = '┓'
RBC = '┛'

IntL = '╋'
UML = '┳'
BML = '┻'
VL = '┃'

def pretty_str(matrix, cw, float_prec = 4, passed_cwv = False):
    if passed_cwv:
        cwv = cw[:]
    else:
        cwv = [ cw for i in matrix[0] ]
    string = top_line(cwv) + '\n'
    cnt = len(matrix)
    for n, row in enumerate(matrix, 1):
        for i, elem in enumerate(row):
            if type(elem) is float:
                string += f"{VL}{elem:^{cwv[i]}.{float_prec}}"
            else:
                string += f"{VL}{str(elem):^{cwv[i]}}"
        string += VL + '\n'
        if cnt != n:
            string += make_line(cwv) + '\n'
        else:
            string += bottom_line(cwv)
    return string

def make_line(cwv, fill_char = HL, begin = LML, sep = IntL, end = RML):
    line = begin
    for i in cwv:
        line += i*fill_char + sep
    line = line[:-1] + end
    return line

def top_line(cwv):
    return make_line(cwv, fill_char = HL, begin = LUC, sep = UML, end = RUC)

def bottom_line(cwv):
    return make_line(cwv, fill_char = HL, begin = LBC, sep = BML, end = RBC)

# print system
def print_system_in_ODF_format(matrix, vector, sign, var_name = 'x', font_size = 12, do_print_zeros= False):
    """
    Get Open Document Formula string from matrix and vector which represent
    an algebraic system.
    """
    string = f"size {font_size} {{ left lbrace alignl matrix{{\n"
    sum_fstring = " {kx} {var_name}_{i} +"
    m_line_sep = ' ##\n'
    string_end = f"\n}} right none }}\nnewline\nsize {font_size} {{{var_name}_i >= 0}}."
    for i, row in enumerate(matrix):
        string += '\t'
        for j, kx in enumerate(row, 1):
            if do_print_zeros or kx != 0:
                lp = '('*(kx < 0)
                rp = ')'*(kx < 0)
                if kx == 1 and not do_print_zeros:
                    kx = ''
                string += sum_fstring.format(i=j, kx=kx, var_name=var_name, lp=lp, rp=rp)
        string = string[:-2] + f" {sign[i]} {vector[i]}" + m_line_sep*(i != len(matrix) - 1)
    string += string_end
    string = string.replace('/', " over ")
    return string
