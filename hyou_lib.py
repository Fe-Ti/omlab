# Copyright 2021 Fe-Ti
# Table library
# "hyou" - in Japanese means "table" or "chart"

HL = '━'

LML = '┣'
LUC = '┏'
LBC = '┗'

RML = '┫'
RUC = '┓'
RBC = '┛'

IntL = '╋'
UML = '┳'
BML = '┻'
VL = '┃'

# ~ HL = '-'

# ~ LML = '+'
# ~ LUC = '+'
# ~ LBC = '+'

# ~ RML = '+'
# ~ RUC = '+'
# ~ RBC = '+'

# ~ IntL = '+'
# ~ UML = '+'
# ~ BML = '+'
# ~ VL = '|'

# ~ HL = ''

# ~ LML = ''
# ~ LUC = ''
# ~ LBC = ''

# ~ RML = ''
# ~ RUC = ''
# ~ RBC = ''

# ~ IntL = ''
# ~ UML = ''
# ~ BML = ''
# ~ VL = '|'

class TableRow:
    def __init__(self, columns = []):
        self.columns = columns[:]
        self.parent_table = ''

    def set_parent_table(self, pt):
        self.parent_table = pt

    def get_column_widths(self, cw = [], precision = 4):
        cw = list()
        for i in self.columns:
            if type(i) is float:
                cw.append(len(f"{i:.{precision}}"))
            else:
                cw.append(len(str(i)))
        return cw

    def length (self):
        return len(self.columns)

    def __str__(self, template = "{val:^{length}}", sep = VL, float_template = "{val:^{length}.{precision}}", precision = 4):
        string = ''
        column_widths = self.parent_table.column_widths

        for i, element in enumerate(self.columns):
            if type(element) is float:
                string += float_template.format(val=element, length=column_widths[i], precision=precision) + sep
            else:
                string += template.format(val=element, length=column_widths[i]) + sep
        return string[:-1]

    def __repr__(self):
        return f"[ {self.__str__(template=' {val}', sep=',')} ]"

    def copy(self):
        c = TableRow(self.columns[:])
        c.set_parent_table(self.parent_table)
        return c

    def __getitem__(self, number):
        return self.columns[number]


class Table:
    def __init__(self, parameters = {
                                        "name"  :   "Table",
                                        "title" :   TableRow(),
                                        "rows"  :   [TableRow()]
                                    }, precision=4):
        """
        Acceptable parameters values are:

        parameters = {
                                        "name"  :   "Some Table Name",
                                        "title" :   [...],
                                        "rows"  :   [[...],...]
                    }

        Or:

        parameters = {
                                        "name"  :   "Some Table Name",
                                        "title" :   TableRow(),
                                        "rows"  :   [TableRow(),...]
                     }
        """
        if type(parameters["title"]) is not TableRow:
            parameters["title"] = TableRow(parameters["title"])
        if type(parameters["rows"][0]) is not TableRow:
            for i, row in enumerate(parameters["rows"]):
                parameters["rows"][i] = TableRow(row)
        self.parameters = parameters.copy()

        self.parameters["title"] = self.parameters["title"].copy()
        for i, row in enumerate(self.parameters["rows"]):
            self.parameters["rows"][i] = row.copy()

        self.precision = precision
        if "name" in parameters:
            self.name = parameters["name"]
        else:
            self.name = "Table"
        if "rows" in parameters:
            self.rows = parameters["rows"][:]
        else:
            self.rows = list()
        if "title" in parameters:
            self.title = parameters["title"]
            self.title.set_parent_table(self)
        else:
            raise Exception("Any table must have a title row")
        # Check if rows have the same length and adjust title length if needed
        if self.rows:
            table_row_len = self.title.length()
            #print(table_row_len)
            for i in self.rows:
                if i.length() != table_row_len:
                    raise Exception("Length of all rows must be the same value")
                i.set_parent_table(self)
        self.column_widths = list()
        self.update_column_widths()

    def get_row(self, keyindex, indexed_column = 0):
        if type(keyindex) is str:
            keyindex = self.get_column(indexed_column).index(keyindex)
        return self.rows[index]

    def get_column(self, keyindex):
        if type(keyindex) is str:
            keyindex = self.title.columns.index(keyindex)
        return [ row[keyindex] for row in self.rows ]

    def update_column_widths(self):
        max_cw = self.title.get_column_widths()
        #print(max_cw)
        for row in self.rows:
            for j, width in enumerate(row.get_column_widths(precision=self.precision)):
                if width > max_cw[j]:
                    max_cw[j] = width
        #print(max_cw)
        self.column_widths = max_cw[:]

    def __str__(self):
        string = self.name + "\n"
        string += self.top_line() + '\n'
        string += VL + str(self.title) + VL + '\n'
        string += self.make_line() + '\n'
        for i in self.rows[:-1]:
            string += VL + str(i) + VL + '\n'
            string += self.make_line() + '\n'
        string += VL + str(self.rows[-1]) + VL + '\n'
        string += self.bottom_line()
        return string

    def make_line(self, fill_char = HL, begin = LML, sep = IntL, end = RML):
        line = begin
        for i in self.column_widths:
            line += i*fill_char + sep
        line = line[:-1] + end
        return line

    def top_line(self):
        return self.make_line(fill_char = HL, begin = LUC, sep = UML, end = RUC)

    def bottom_line(self):
        return self.make_line(fill_char = HL, begin = LBC, sep = BML, end = RBC)

    def copy(self):
        return Table(self.parameters)

    def __getitem__(self, key):
        if key == "title":
            return self.title
        return self.rows[key]
    
    def to_list(self, with_row_names = False):
        matrix = []
        for row in self.rows:
            if not with_row_names:
                matrix.append(row.columns[1:])
            else:
                matrix.append(row.columns)
            #print(matrix[-1])
        return matrix
        
    def get_matrix(self):
        return self.to_list(with_row_names=False)
        
    def append_row(self, row):
        if type(row) is not TableRow:
            row = TableRow(row)
        self.rows.append(row.copy)
