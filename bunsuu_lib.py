from __future__ import division
# Copyright 2021 Fe-Ti
# Fraction Lib

def greatest_divisor(a, b):
    while b!=0:
        t = b
        b = a % b
        a = t
    return a

def least_multiple(a, b):
    return (a*b)/greatest_divisor(a,b)

class Fraction():
    num = 0 # num
    den = 1 # denominator
    def get_num(self):
        return self.num
    def get_den(self):
        return self.den

    def __init__(self, num = 0, den = 1):
        if type(num) == type('string'):
            if (num.find('/')!=-1):
                den = float(num.split('/')[1])
                num = float(num.split('/')[0])
            else:
                num = float(num)
        while num != int(num): # making int if we've got smth like 0.112/29.1
            num *= 10
            den *= 10
        while den != int(den):
            den *= 10
            num *= 10
        self.num = int(num)
        self.den = int(den)
        self.reduce_fraction()

    def reduce_fraction(self):
        div = greatest_divisor(self.num, self.den)
        self.num = int(self.num / div)
        self.den = int(self.den / div)

    def parse(self, string = '0/1'):
        string = string.split('/')
        self.num = float(string[0])
        if len(string) == 2:
            self.den = float(string[1])

    def __str__(self):
        if self.den == 1:
            return str(self.num)
        else:
            num_str = str(self.num)
            den_str = str(self.den)
            string = num_str+'/'+den_str
            return string

    def __repr__(self):
        return str(self)

    def __float__(self):
        return self.num/self.den

    def to_string(self, force_dec = True):
        if not force_dec:
            return str(self)
        else:
            return str(float(self))

    def __add__(self, other):
        if not type(other) is type(Fraction()):
            other = Fraction(other)
        if(self.den != other.den):
            newden = int(least_multiple(self.den, other.den))
            newnum = int(self.num * (newden / self.den) + other.num * (newden / other.den))
        else:
            newden = self.den
            newnum = self.num + other.num
        newFraction = Fraction(newnum, newden)
        return newFraction

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        if not type(other) is type(Fraction()):
            other = Fraction(other)
        if(self.den != other.den):
            newden = int(least_multiple(self.den, other.den))
            newnum = int(self.num * (newden / self.den) - other.num * (newden / other.den))
        else:
            newden = self.den
            newnum = self.num - other.num
        newFraction = Fraction(newnum, newden)
        return newFraction

    def __rsub__(self, other):
        if not type(other) is type(Fraction()):
            other = Fraction(other)
        if(self.den != other.den):
            newden = int(least_multiple(self.den, other.den))
            newnum = int(other.num * (newden / other.den) - self.num * (newden / self.den))
        else:
            newden = self.den
            newnum = other.num - self.num
        newFraction = Fraction(newnum, newden)
        return newFraction

    def __neg__(self):
        newFraction = Fraction(-self.num, self.den)
        return newFraction

    def __mul__(self, other):
        if not type(other) is type(Fraction()):
            other = Fraction(other)
        newFraction = Fraction(self.num * other.num, self.den * other.den)
        return newFraction

    def __rmul__(self, other):
        if not type(other) is type(Fraction()):
            other = Fraction(other)
        newFraction = Fraction(self.num * other.num, self.den * other.den)
        return newFraction

    def __div__(self, other):
        if not type(other) is type(Fraction()):
            other = Fraction(other)
        newFraction = Fraction(self.num * other.den, self.den * other.num)
        if newFraction.den == 0:
            raise ZeroDivisionError("division by zero")
        return newFraction

    def __truediv__(self, other):
        if not type(other) is type(Fraction()):
            other = Fraction(other)
        newFraction = Fraction(self.num * other.den, self.den * other.num)
        if newFraction.den == 0:
            raise ZeroDivisionError("division by zero")
        return newFraction

    def __rdiv__(self, other):
        if not type(other) is type(Fraction()):
            other = Fraction(other)
        newFraction = Fraction(self.den * other.num, self.num * other.den)
        if newFraction.den == 0:
            raise ZeroDivisionError("division by zero")
        return newFraction

    def __rtruediv__(self, other):
        if not type(other) is type(Fraction()):
            other = Fraction(other)
        newFraction = Fraction(self.den * other.num, self.num * other.den)
        if newFraction.den == 0:
            raise ZeroDivisionError("division by zero")
        return newFraction

    def compare(self, other):
        dec_frac_s = self.num / self.den
        if type(other) is type(Fraction()):
            dec_frac_o = other.num / other.den
        else:
            dec_frac_o = other
        return dec_frac_s - dec_frac_o

    def __eq__(self, other):
        if self.compare(other) == 0:
            return True
        else:
            return False

    def __ne__(self, other):
        if self.compare(other) != 0:
            return True
        else:
            return False

    def __lt__(self, other):
        if self.compare(other) < 0:
            return True
        else:
            return False

    def __gt__(self, other):
        if self.compare(other) > 0:
            return True
        else:
            return False

    def __le__(self, other):
        if self.compare(other) <= 0:
            return True
        else:
            return False

    def __ge__(self, other):
        if self.compare(other) >= 0:
            return True
        else:
            return False

# Copyright 2021 Fe-Ti
