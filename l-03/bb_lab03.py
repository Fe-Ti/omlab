# Lab 03
# Copyright 2021 Fe-Ti
# 2B method
from bb_method import BBSolver
from udezuku_lib import BForcer
from bunsuu_lib import Fraction

# ~ test_parameters = {
        # ~ "func vector"   :   [Fraction(0), Fraction(12), Fraction(-1)],
        # ~ "lim matrix"    :   [
                                # ~ [Fraction(6), Fraction(-1)],
                                # ~ [Fraction(2),  Fraction(5)],
                            # ~ ],
        # ~ "lim vector"    :   [Fraction(12), Fraction(20)],
        # ~ "find max"      :   True,
    # ~ }

# ~ test = BBSolver(test_parameters)
# ~ test.solve(retry_count=-1)
# ~ test.print_solution_tree()
# ~ print("BBS's best solution:", test.get_best_solution())
# ~ test_bf = BForcer(test_parameters, debug=False)
# ~ print("QC solution from BF:", test_bf)

# ~ lab_parameters = {
        # ~ "func vector"   :   [0, 2,5, 3],
        # ~ "lim matrix"    :   [
                                # ~ [ 2,  1, 2],
                                # ~ [ 1, 2, 0],
                                # ~ [ 0,  0.5, 1],
                            # ~ ],
        # ~ "lim vector"    :   [
                                # ~ 6,
                                # ~ 6,
                                # ~ 2,
                            # ~ ],
            # ~ "find max"      :   True
# ~ }

lab_parameters = {
        "func vector"   :   [Fraction(0), Fraction(2),Fraction(5), Fraction(3)],
        "lim matrix"    :   [
                                [ Fraction(2),  Fraction(1), Fraction(2)],
                                [ Fraction(1), Fraction(2), Fraction(0)],
                                [ Fraction(0),  Fraction('1/2'), Fraction(1)],
                            ],
        "lim vector"    :   [
                                Fraction(6),
                                Fraction(6),
                                Fraction(2),
                            ],
            "find max"      :   True
}

lab_bbs = BBSolver(lab_parameters)
lab_bbs.solve()
print("\nQC solution from BF:", BForcer(lab_parameters, debug=True))
print("BBS's best solution:", lab_bbs)
print("\nBBS's solution tree:")
lab_bbs.print_solution_tree()
# Copyright 2021 Fe-Ti
