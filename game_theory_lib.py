# Copyright 2021 Fe-Ti
# Game theory library
from gyouretsu_lib import * #make_matrix_copy, transpose_matrix, dump_matrix_to_str, pretty_str, print_system_in_ODF_format
from bb_method import LPTask

class AntagonisticGame_P2:
    def __init__(self, ab_table, solve_on_creation = True, debug = True):
        '''
        A table:
           | b_1 | b_2
        a_1| c_11| c_12
        a_2| c_21| c_22

        is equal to 2D list like:

        ab_table = [
            [c11, c12],
            [c21, c22],
        ]
        '''
        self.debug = debug
        self.table = make_matrix_copy(ab_table)
        self.solution = dict()
        self.systems = dict()
        self.solution = dict()
        if solve_on_creation:
            self.solve()

    def log(self, msg):
        if self.debug:
            print(msg,"\n")

    def make_printable_systems_for_A(self, t_table):
        self.systems["A_0"] = {
                                "matrix" : t_table + [[ 1 for i in t_table[0] ]],
                                "vector" : [ 'g' for i in t_table ] + [ 1 ],
                                "sign"   : [ '>='  for i in t_table ] + [ '=' ],
                                "var_name": 'x'
                              }
        self.log(print_system_in_ODF_format(**self.systems["A_0"]))
        self.systems["A_1"] = {
                                "matrix" : self.systems["A_0"]["matrix"],
                                "vector" : [ 1 for i in t_table ] + [ '1/g' ],
                                "sign"   : [ '>='  for i in t_table ] + [ '=' ],
                                "var_name": 'u'
                              }
        self.log(print_system_in_ODF_format(**self.systems["A_1"]))

    def make_printable_systems_for_B(self, table):
        self.systems["B_0"] = {
                                "matrix" : table + [[ 1 for i in table[0] ]],
                                "vector" : [ 'h' for i in table ] + [ 1 ],
                                "sign"   : [ '<='  for i in table ] + [ '=' ],
                                "var_name": 'y'
                              }
        self.log(print_system_in_ODF_format(**self.systems["B_0"]))
        self.systems["B_1"] = {
                                "matrix" : self.systems["B_0"]["matrix"],
                                "vector" : [ 1 for i in table ] + [ '1/h' ],
                                "sign"   : [ '<='  for i in table ] + [ '=' ],
                                "var_name": 'v'
                              }
        self.log(print_system_in_ODF_format(**self.systems["B_1"]))

    def solve(self):
        # looking at A strategies
        t_table = transpose_matrix(self.table)
        self.make_printable_systems_for_A(t_table)
        lp_parameters_a = {
            "func vector"   :   [0] + [ 1 for i in t_table[:-1] ],
            "lim matrix"    :   multiplicate(-1, t_table),
            "lim vector"    :   [ -1 for i in t_table ],
            "find max"      :   False,
            "branching_var" :   None,
            "var name"      :   'u'
        }
        linear_task_A = LPTask(lp_parameters_a, debug=self.debug, print_solution=False)
        linear_task_A.solve(-1)
        linear_task_A.print_solution_string()
        self.solution['g'] = g = 1 / linear_task_A.solution['F']
        self.solution['optimal strategy A'] = [ g * linear_task_A.solution['X'][i] for i in linear_task_A.solution['X']]
        self.solution['linear task A'] = linear_task_A

        # looking at B strategies
        table = self.table
        self.make_printable_systems_for_B(table)
        lp_parameters_b = {
            "func vector"   :   [0] + [ 1 for i in table[0] ],
            "lim matrix"    :   table,
            "lim vector"    :   [ 1 for i in table ],
            "find max"      :   True,
            "branching_var" :   None,
            "var name"      :   'v'
        }
        linear_task_B = LPTask(lp_parameters_b, debug=self.debug, print_solution=False)
        linear_task_B.solve(-1)
        linear_task_B.print_solution_string()
        self.solution['h'] = h = 1 / linear_task_B.solution['F']
        self.solution['optimal strategy B'] = [ h * linear_task_B.solution['X'][i] for i in linear_task_B.solution['X']]
        self.solution['linear task B'] = linear_task_B

    def get_quality_check_string(self, optimal_strategy, var_name):
        chsum = 0
        substrN = ''
        substrV = ''
        string = ''
        for i, s in enumerate(optimal_strategy, 1):
            chsum += s
            substrN += f"{var_name}_{i} + "
            substrV += f"{s} + "
        string += f"Quality check:\n{substrN[:-2]}= {substrV[:-2]}= {chsum}\n"
        string += f"The solution is{''''nt'''*(chsum != 1)} correct."
        return string

    def __str__(self, width = 5):
        table = [[f"b{i}" for i, elem in enumerate(self.table[0])] ] + make_matrix_copy(self.table)
        table[0] = ["*"] + table[0]
        for i, row in enumerate(table[1:],1):
            table[i] = [f"a{i}"] + row
        string = "\nPayoff Matrix:\n" + pretty_str(table, width) + "\n"
        string += "\nSystem for getting player A optimal strategy:\n" + print_system_in_ODF_format(**self.systems["A_0"]) + "\n"
        string += "\nThe same devided by function g:\n" + print_system_in_ODF_format(**self.systems["A_1"]) + "\n"
        string += "\nCanonical LP task form:\n" + self.solution['linear task A'].get_canonical_task_form_string() + "\n"
        string += "\nThe LP solution:\n" + self.solution['linear task A'].get_solution_string() + "\n"
        string += "\ng = " + f"{self.solution['g']}" + "\n"
        string += f"\nThe corresponding strategy: {self.solution['optimal strategy A']}\n"
        string += f"{self.get_quality_check_string(self.solution['optimal strategy A'], self.systems['A_0']['var_name'])}\n"
        string += "\nSystem for getting player B optimal strategy:\n" + print_system_in_ODF_format(**self.systems["B_0"]) + "\n"
        string += "\nThe same devided by g()\n" + print_system_in_ODF_format(**self.systems["B_1"]) + "\n"
        string += "\nCanonical LP task form:\n" + self.solution['linear task B'].get_canonical_task_form_string() + "\n"
        string += "\nThe LP solution:\n" + self.solution['linear task B'].get_solution_string() + "\n"
        string += "\nh = " + f"{self.solution['h']}" + "\n"
        string += f"\nThe corresponding strategy:\n{self.solution['optimal strategy B']}\n"
        string += f"{self.get_quality_check_string(self.solution['optimal strategy B'], self.systems['B_0']['var_name'])}"
        return string.replace('/', " over ")
