# Lab 02
# Copyright 2021 Fe-Ti
# Simplex Algorithm & Abivalent Problem

from bunsuu_lib import Fraction
from ambivalent_problem_lib import make_ambivalent_problem
import canonical_finder_lib as cfl

# Sample task
# ~ params = (
            # ~ [Fraction(-4), Fraction(-18), Fraction(-30), Fraction(-5)],
            # ~ [
                # ~ [Fraction(3),Fraction(1),Fraction(-4),Fraction(-1)],
                # ~ [Fraction(-2),Fraction(-4),Fraction(-1),Fraction(1)],
            # ~ ],
            # ~ [
                # ~ Fraction(-3),
                # ~ Fraction(-3),
            # ~ ],
            # ~ True
            # ~ )
params = (
            [Fraction(2),Fraction(5), Fraction(3)],
            [
                [ Fraction(2),  Fraction(1), Fraction(2)],
                [ Fraction(1), Fraction(2), Fraction(0)],
                [ Fraction(0),  Fraction('1/2'), Fraction(1)],
            ],
            [
                Fraction(6),
                Fraction(6),
                Fraction(2),
            ],
            True
            )

amb_params = make_ambivalent_problem(params[0],params[1],params[2],params[3])
print('\n\nExample with stock types (int + float)\n\n')

cfn = cfl.CanonicalFinder([Fraction(0)] + params[0],params[1],params[2],params[3])
cfn.make_simplex_table()
cfn.solve_and_print()

print('\n\nAmbivalent\n\n')
print("Function vector:")
for i in amb_params[0]:
    print (i)
print("Limiting matrix:")
for i in amb_params[1]:
    string = ''
    for j in i:
        string += f"{str(j):>5}"
    print(string)
    
print("Limiting vector:")
for i in amb_params[2]:
    print (i)
print("Find max:",amb_params[3])

cfn2 = cfl.CanonicalFinder([Fraction(0)] + amb_params[0],amb_params[1],amb_params[2],amb_params[3])
cfn2.make_simplex_table()
cfn2.solve_and_print()

# Copyright 2021 Fe-Ti
