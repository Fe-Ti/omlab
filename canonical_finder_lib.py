from simplex_algorithm_lib import *
# Copyright 2021 Fe-Ti
# Canonical Solution Finder

class CanonicalFinder: 
    debug = True
    '''
    CanonicalFinder can solve such problems as:
        F = s + cx, Ax<=b, F-->min (or F-->max depending on parameters)
    Where:
        s + cx      - function vector
        Ax          - limiting (constraints) matrix
        b           - limiting vector
    Accepted parameters:
        func_vector - function vector
        lim_matrix  - limiting (constraints) matrix
        lim_vector  - limiting vector
        find_max    - pass True to find function maximum value
    '''

    def __init__(self, func_vector, # s + c
                        lim_matrix, # A
                        lim_vector, # b
                        find_max = False,
                        debug = True):
        self.simplex_table = list()
        self.find_max = find_max
        self.debug = debug

        if find_max:    # inverting values
            self.func_vector = [-i for i in func_vector]
        else:
            self.func_vector = func_vector[:]
        self.lim_matrix = make_matrix_copy(lim_matrix)
        self.lim_vector = lim_vector[:]

    def log(self,string):
        '''(Internal function)
        Print string if self.debug is True.
        '''
        if self.debug:
            print(string)

    def make_simplex_table(self):
        '''
        Make simplex tableau.
        '''
        mx = make_matrix_copy(self.lim_matrix)

        self.log("Matrix after copying\n" + dump_matrix_to_str(mx))

        for i,row in enumerate(mx):
            mx[i] = [self.lim_vector[i]] + row[:]

            self.log("Extracting basic vars\n" + dump_matrix_to_str(mx))

        mx.append([-j for j in self.func_vector])

        self.log("Appended function vector\n" + dump_matrix_to_str(mx))

        copy_matrix(mx, self.simplex_table)

        self.log("Simplex table is now:\n" + dump_matrix_to_str(self.simplex_table))

    def solve(self, retry_count = 0):
        '''
        Solve the simplex tableau.
        Run make_simplex_table() method before calling this function.
        '''
        try:
            self.ss = SimplexSolver(self.simplex_table, self.debug)
            self.ss.solve(retry_count)
        except SolverException as e:
            raise SolverException(str(e))

    def get_func_extremum(self):
        '''
        Get function extremum.
        '''
        if self.find_max:
            return -self.ss.func_extremum
        else:
            return self.ss.func_extremum

    def get_basic_vars(self):
        '''
        Get basic variables including slack ones.
        '''
        return self.ss.get_basic_vars()

    def get_function_vars(self):
        '''
        Filter basic variables by being slack, e.i. the ones
        which are presented in function.
        Also returns variables which are equal to zero.
        '''
        var_count = len(self.func_vector)
        func_vars = dict()
        basic_vars = self.get_basic_vars()
        for i in range(1, var_count):
            if str(i) in basic_vars:
                func_vars[str(i)] = basic_vars[str(i)]
            else:
                func_vars[str(i)] = 0
        return func_vars

    def solve_and_print(self):
        '''
        Solve and print the result.
        This is just solve() with printing to console.
        '''
        try:
            self.solve()
            print("\nSolution:\n")
            print(self.ss.dump_mx(self.find_max))

            print("All basic variables are:")
            basic_vars = self.get_basic_vars()
            for elem in basic_vars:
                print("\tX_", elem, " = ", basic_vars[elem], sep='')

            print("And variables in function are:")
            fnc_vars = self.get_function_vars()
            for elem in fnc_vars:
                print("\tX_", elem, " = ", fnc_vars[elem], sep='')

            print("Function extremum is:")
            print("\tF = ", self.get_func_extremum())

        except SolverException as error_msg:
            print("ERROR:", error_msg)

