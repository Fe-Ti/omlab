# Lab 05
# Copyright 2021 Fe-Ti
# Signal filtration

import math
import random
import sys
from gyouretsu_lib import pretty_str
from hyou_lib import Table

def get_xk (k, K, x_min, x_max):
    return x_min + k*(x_max - x_min)/K

def get_fk (xk):
    return math.sin(xk) + 0.5

def get_M(r):
    return r // 2

def get_filtered(noisy_f_k, weights, r, K): # geometric average
    filtered = [ 1 for i in noisy_f_k ]
    M = get_M(r)
    k_list = [ i for i in range(M, K - M + 1) ]
    
    for k in k_list:
        for j in range(k - M, k + M + 1):
            #print(k, j, j + M + 1 - k)
            filtered[k] *= (noisy_f_k[j] ** (weights[j + M - k]))
    return filtered

def get_delta(K, filtered_f_k, noisy_f_k): # difference
    delta = 0
    for i in k:
        delta += abs( filtered_f_k[i] - noisy_f_k[i] )
    return (1/K) * delta
    
def get_omega(k, filtered_f_k): # noisiness
    omega = 0
    for i in k[1:]:
        omega += abs(filtered_f_k[i] - filtered_f_k[i - 1])
    return omega

def get_J (r, K, k, lambda_l, f_k, noisy_f_k, weights):
    filtd = get_filtered(noisy_f_k, weights, r, K)
    return lambda_l*get_omega(k, filtd) + (1 - lambda_l) * get_delta(K, filtd, noisy_f_k)

def get_weights(r):
    M = get_M(r)
    weights = [ random.random() ]
    for i in range(M - 1):
        nw = 0.5 * random.uniform(0, 1 - sum(weights))
        weights = [ nw ] + weights + [ nw ]
    nw = 0.5 * (1 - sum(weights))
    weights = [ nw ] + weights + [ nw ]
    return weights

def rnd_search(r, N, eps, K, k, lambda_l, f_k, noisy_f_k):
    counter = 0
    diff = 1
    weights = []
    best_weights = []
    counter = 0
    oldJ = -1
    while abs(diff) > eps:
        counter += 1
        weights = get_weights(r)
        newJ = get_J(r, K, k, lambda_l, f_k, noisy_f_k, weights)
        diff = oldJ - newJ
        if (diff > 0 and best_weights!=weights) or oldJ == -1:
            oldJ = newJ
            best_weights = weights[:]
            counter = 0
        if counter > N:
            break
    return best_weights

def dist(w, d):
    return abs(w) + abs(d)

def is_better(old, new):
    return dist(new["w"], new["d"]) < dist(old["w"], old["d"])

def flist_to_str(flist, fprec=4):
    string = f"[{flist[0]:.{fprec}f}"
    for i in flist[1:]:
        string += f", {i:.{fprec}f}"
    return string + "]"

def search(r, N, eps, K, k, f_k, noisy_f_k, lambda_l_list):
    best = dict()
    mx = []
    mxwd = []
    for lml in lambda_l_list:
        optimal_w = rnd_search(r, N, eps, K, k, lml, f_k, noisy_f_k)
        J = get_J(r, K, k, lml, f_k, noisy_f_k, optimal_w)
        filtered = get_filtered(noisy_f_k, optimal_w, r, K)
        d = get_delta(K, filtered, noisy_f_k)
        w = get_omega(k, filtered)
        distance = dist(w,d)
        new = {
                    "w":        w,
                    "d":        d,
                    "weights":  optimal_w,
                    "lambda_l": lml,
                    "filtered": filtered,
                    "J":        J,
            }
        mx.append([lml, distance, flist_to_str (optimal_w[:]), w, d])
        mxwd.append([w, d])
        if not best:
            best = new.copy()
        elif is_better(best, new):
            best = new.copy()
    t = Table(
                {
                    "name"  :   f"Результат эксперимента для r = {r}",
                    "title" :   ["h", "dis", "alpha", "w", "d"],
                    "rows"  :   mx
                }
            )
    sub_t = Table(
                {
                    "name"  :   f"Оптимальное значение веса λ*, функционала J и критериев δ, ω  при r = {r}",
                    "title" :   ["h*", "J", "w", "d"],
                    "rows"  :   [
                                    [ best["lambda_l"],
                                    best["J"],
                                    best["w"],
                                    best["d"] ]
                                ]
                }
            )

    return best, mx, t, sub_t, mxwd

#-------------------------------------------------
K = 100
k = [ i for i in range(K + 1) ]
#print(k)
x_min = 0
x_max = math.pi

a = 0.25

x_k = [ get_xk(i, K, x_min, x_max) for i in k ]
f_k = [ get_fk(i) for i in x_k ]

noise = [ (random.random() % (2*a)) - a for i in f_k ]

noisy_f_k = [ f_k[i] + noise_k for i,noise_k in enumerate(noise) ]

L = 10
l = [ i for i in range(L + 1) ]
lambda_l_list = [ i / L for i in l ] # weight discretisation

P = 0.95 # probability
eps = 0.01

ln = math.log
N = math.floor(ln(1 - P)/ln(1 - (eps)/(x_max - x_min)))
#-------------------------------------------------
rs = [3,5] # window sizes
bs = dict()
mxwd = dict()
r = rs[0]
bs[3], mx, t_r3, subt_r3, mxwd[3] = search(r, N, eps, K, k, f_k, noisy_f_k, lambda_l_list)
r = rs[1] # window size
bs[5], mx, t_r5, subt_r5, mxwd[5] = search(r, N, eps, K, k, f_k, noisy_f_k, lambda_l_list)
print(t_r3, subt_r3, sep="\n")
print(t_r5, subt_r5, sep="\n")

lstring = str([ f"λ_{{{i}}}" for i in range(1, 11)])
lstring = lstring.replace('[','').replace(']','')

if input("Do you want to save plots? ") in ["y","Y","yes","YES","Yes",]:
    for r in rs:
        strmxwd = str(mxwd[r]).replace('[','').replace(']','')
        with open(f"Matlab_plot_r_{r}.m", "w") as out:
            string = f"""
X = {x_k};
F = {f_k};
Noisy = {noisy_f_k};
Filtering = {bs[r]["filtered"]};
H = plot(
X, F, 'k',
X, Noisy, 'r',
X, Filtering, 'b'
)
set(H, "linewidth", 1.5);
legd = legend("Original f(x) = sin(x) + 0.5", "Noisy f(x)", "Filtered f(x)");
set (legd, "fontsize", 12);
set (legd, "location", "southoutside");
title("Results with r = {r}");
xlabel("x");
ylabel("f(x)");
figure;
H2 = plot({strmxwd});
legd2 = legend({lstring});
set (legd2, "fontsize", 12)
set (legd2, "location", "northeastoutside");
set(H2, "linewidth", 13);
set(H2, "MarkerSize", 20);
xlabel("ω")
ylabel("δ")
"""
            out.write(string)
#-------------------------------------------------
