from gyouretsu_lib import transpose_matrix
# Copyright 2021 Fe-Ti
# Ambivalent Problem Lib

def make_ambivalent_problem(func_vector, # s + c
                            lim_matrix, # A
                            lim_vector, # b
                            find_max = False):
    lim_vector = lim_vector
    lim_matrix = transpose_matrix(lim_matrix)
    for i, row in enumerate(lim_matrix):
        for j, elem in enumerate(row):
            lim_matrix[i][j] = -elem

    func_vector = [-i for i in func_vector]
    find_max = not find_max
    return (lim_vector, lim_matrix, func_vector, find_max)
